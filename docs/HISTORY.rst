Changelog
=========

2.0.1
-----
 - upgraded to Pyramid 2.0

1.1.2
-----
 - added support for Python 3.10 and 3.11

1.1.1
-----
 - added exception handler to check for ZMQ errors in process startup and end process
   if an exception occurs (for example if the requested port is already used)

1.1.0
-----
 - removed support for Python < 3.7

1.0.3
-----
 - updated Gitlab-CI configuration
 - removed Travis-CI configuration

1.0.2
-----
 - added ZCA hook in doctests

1.0.1
-----
 - updated doctests

1.0.0
-----
 - initial release
